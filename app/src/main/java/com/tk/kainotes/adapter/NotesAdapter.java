package com.tk.kainotes.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tk.kainotes.R;
import com.tk.kainotes.model.SynopsisNotes;

import java.util.List;

/**
 * Created by GEC-IOS100 on 15/11/3.
 */
public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.NotesViewHolder>{

    Context context;
    List<SynopsisNotes> list;

    public NotesAdapter(Context context,List<SynopsisNotes> list){
        this.context = context;
        this.list = list;
    }

    @Override
    public NotesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        NotesViewHolder holder = new NotesViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_notes_recylerview_item,parent,false));
        return holder;
    }

    @Override
    public void onBindViewHolder(NotesViewHolder holder, int position) {
        String dateStr = list.get(position).getDate();
        String synsStr = list.get(position).getSynopsis();
        if (dateStr != null && !TextUtils.isEmpty(dateStr)){
            holder.dateTextView.setText(dateStr);
        }
        if (synsStr != null && !TextUtils.isEmpty(synsStr)){
            holder.synopsisTextView.setText(synsStr);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class NotesViewHolder extends RecyclerView.ViewHolder{

    private TextView  synopsisTextView;
    private TextView dateTextView;

    public NotesViewHolder(View view){
        super(view);
        synopsisTextView = (TextView) view.findViewById(R.id.item_synopsis);
        dateTextView = (TextView) view.findViewById(R.id.item_date);
    }
}

}
