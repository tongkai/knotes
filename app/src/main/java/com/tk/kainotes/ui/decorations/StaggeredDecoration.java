package com.tk.kainotes.ui.decorations;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;


public class StaggeredDecoration extends RecyclerView.ItemDecoration
{

    private static final int[] ATTRS = new int[] { android.R.attr.listDivider };
    private Drawable mDivider;

    public StaggeredDecoration(Context context)
    {
        final TypedArray a = context.obtainStyledAttributes(ATTRS);
        mDivider = a.getDrawable(0);
        a.recycle();
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state)
    {

        drawHorizontal(c, parent);
        drawVertical(c, parent);

    }

    private int getSpanCount(RecyclerView parent)
    {
        // 列数
        int spanCount = -1;
        RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
        if (layoutManager instanceof GridLayoutManager)
        {
            spanCount = ((GridLayoutManager) layoutManager).getSpanCount();

        } else if (layoutManager instanceof StaggeredGridLayoutManager)
        {
            spanCount = ((StaggeredGridLayoutManager) layoutManager)
                    .getSpanCount();
        }
        return spanCount;
    }

    public void drawHorizontal(Canvas c, RecyclerView parent)
    {
        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++)
        {
            final View child = parent.getChildAt(i);

            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                    .getLayoutParams();
            final int left = child.getLeft() - params.leftMargin;
            final int right = child.getRight() + params.rightMargin
                    + mDivider.getIntrinsicWidth();
            final int top = child.getBottom() + params.bottomMargin;
            final int bottom = top + mDivider.getIntrinsicHeight();
            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }

    public void drawVertical(Canvas c, RecyclerView parent)
    {
        final int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++)
        {
            final View child = parent.getChildAt(i);

            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                    .getLayoutParams();
            final int top = child.getTop() - params.topMargin;
            final int bottom = child.getBottom() + params.bottomMargin;
            final int left = child.getRight() + params.rightMargin;
            final int right = left + mDivider.getIntrinsicWidth();

            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }

    private boolean isFirstColum(int pos, int spanCount)
    {
        if (pos % spanCount == 0)
        {
            return true;
        }

        return false;
    }

    private boolean isLastColum(int pos, int spanCount)
    {
        if ((pos + 1) % spanCount == 0)
        {
            return true;
        }

        return false;
    }

    private boolean isFirstRaw(int pos, int spanCount, int childCount)
    {
        return pos == pos % spanCount;
    }

    private boolean isLastRaw(int pos, int spanCount, int childCount)
    {
        return childCount == pos + (spanCount - pos % spanCount);

    }


    @Override
    public void getItemOffsets(Rect outRect, int itemPosition,
                               RecyclerView parent)
    {

        int spanCount = getSpanCount(parent);
        int childCount = parent.getAdapter().getItemCount();


        if (isFirstRaw(itemPosition, spanCount, childCount) && isLastColum(itemPosition, spanCount)){
            //第一行最后一个绘制  上左右下
            outRect.set(mDivider.getIntrinsicWidth(), mDivider.getIntrinsicHeight(), mDivider.getIntrinsicWidth(),  mDivider.getIntrinsicHeight());

        }else if (isFirstRaw(itemPosition, spanCount, childCount) && !isLastColum(itemPosition, spanCount)){
            //第一行其他绘制 上左下
            outRect.set(mDivider.getIntrinsicWidth(),  mDivider.getIntrinsicHeight(), 0,mDivider.getIntrinsicHeight());

        }else if (isLastColum(itemPosition, spanCount) && !isFirstRaw(itemPosition, spanCount, childCount)){
            //除了第一行最后一个，最后一列绘制 左右下
            outRect.set(mDivider.getIntrinsicWidth(),0, mDivider.getIntrinsicWidth(),mDivider.getIntrinsicHeight());

        }else {
            //其他绘制 左下
            outRect.set( mDivider.getIntrinsicWidth(),0,0,mDivider.getIntrinsicHeight());

        }

    }
}
