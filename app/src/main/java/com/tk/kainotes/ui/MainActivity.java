package com.tk.kainotes.ui;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.tk.kainotes.R;
import com.tk.kainotes.ui.fragments.AboutFragment;
import com.tk.kainotes.ui.fragments.NotesFragment;
import com.tk.kainotes.ui.fragments.RecycleFragment;
import com.tk.kainotes.ui.fragments.SettingFragment;
import com.tk.kainotes.utils.ToolbarUtil;

public class MainActivity extends AppCompatActivity {

    private static final int NOTE = 1;
    private static final int RECYCLE = 2;
    private static final int ABOUT = 3;
    private static final int SETTING = 4;

    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private RecyclerView mRecycleView;
    private ActionBarDrawerToggle mToggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


       mToolbar = (Toolbar) findViewById(R.id.id_toolbar);
//        setSupportActionBar(mToolbar);
//        mToolbar.setTitle(R.string.app_name);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);//给home icon的左边加上一个返回的图标
//        getSupportActionBar().setHomeButtonEnabled(true);//需要api level 14  使用home-icon 可点击

        ToolbarUtil.initToolbar(this,mToolbar);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.id_drawer);
        mNavigationView = (NavigationView) findViewById(R.id.navigation);

        mToggle = new ActionBarDrawerToggle(this,mDrawerLayout,R.string.str_open,R.string.str_close){
            @Override
            public void onDrawerOpened(View drawerView) {// 打开drawer
                super.onDrawerOpened(drawerView);
               // mToggle.onDrawerOpened(drawerView);//开关状态改为opened
            }

            @Override
            public void onDrawerClosed(View drawerView) {// 关闭drawer
                super.onDrawerClosed(drawerView);
               // mToggle.onDrawerClosed(drawerView);//开关状态改为closed
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {// drawer滑动的回调
                super.onDrawerSlide(drawerView,slideOffset);
               // mToggle.onDrawerSlide(drawerView, 0);
            }

            @Override
            public void onDrawerStateChanged(int newState) {// drawer状态改变的回调
                super.onDrawerStateChanged(newState);
                //mToggle.onDrawerStateChanged(newState);
            }

        };

        initFragment(NOTE);


        mDrawerLayout.setDrawerListener(mToggle);
        mDrawerLayout.setScrimColor(R.color.drawer_scrim_color);

        /**
         * 关于NavigationView中item的字体颜色和icon选中状态颜色是去当前主题theme中的
         * <--正常状态下字体颜色和icon颜色-->
         *  <item name="android:textColorPrimary">@android:color/darker_gray</item>
         *  <--选中状态icon的颜色和字体颜色-->
         *  <item name="colorPrimary">@color/accent_material_light</item>
         *  setItemBackgroundResource(int)：给menu设置背景资源，对应的属性app:itemBackground
         *  setItemIconTintList(ColorStateList)：给menu的icon设置颜色，对应的属性app:itemIconTint
         *  setItemTextColor(ColorStateList)：给menu的item设置字体颜色，对应的属性app:itemTextColor
         */
        mNavigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {

                    //点击不同fragment时，改变toolbar文字和布局
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.menu_notes:
                                if (!menuItem.isChecked()){
                                    initFragment(NOTE);
                                }
                                break;
                            case R.id.menu_recycle_bin:
                                if (!menuItem.isChecked()){
                                    initFragment(RECYCLE);
                                }

                                break;
                            case R.id.menu_setting:
                                if (!menuItem.isChecked()){
                                    initFragment(SETTING);
                                }

                                break;
                            case R.id.menu_about:
                                if (!menuItem.isChecked()){
                                    initFragment(ABOUT);
                                }

                                break;
                            case R.id.menu_exit:

                                break;

                        }
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        return true;
                    }
                }

        );

    }
    //下面是ActionBarDrawerToggle三部曲
    /* 设备配置改变时 */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mToggle.onConfigurationChanged(newConfig);
    }
    /* activity创建完成后 */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        //该方法会自动和actionBar关联, 将开关的图片显示在了action上，如果不设置，也可以有抽屉的效果，不过是默认的图标
        mToggle.syncState();
    }

    /*设置菜单的处理*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    private void initFragment(int type){
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        switch(type){
            case NOTE:
                ft.replace(R.id.fragment_container,new NotesFragment());
                break;
            case RECYCLE:
                ft.replace(R.id.fragment_container,new RecycleFragment());
                break;
            case ABOUT:
                ft.replace(R.id.fragment_container,new AboutFragment());
                break;
            case SETTING:
                ft.replace(R.id.fragment_container,new SettingFragment());
                break;
        }
        ft.commit();
    }


}
