package com.tk.kainotes.ui.fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tk.kainotes.R;
import com.tk.kainotes.adapter.RecycleAdapter;
import com.tk.kainotes.model.SynopsisNotes;
import com.tk.kainotes.ui.decorations.StaggeredDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecycleFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private RecycleAdapter mAdapter;

    private List<SynopsisNotes> list;

    public RecycleFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycle, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycle_recyclerview);
        mAdapter = new RecycleAdapter(getActivity(),list);
        mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL));
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(new StaggeredDecoration(this.getActivity()));
        return view;
    }

    private void initData(){
        list = new ArrayList<SynopsisNotes>();
        SynopsisNotes synNotes = null;
        for (int i = 0;i < 20;i++){
            synNotes = new SynopsisNotes("TAG"+i,"2015-09-"+(i+10));
            list.add(synNotes);
        }
    }
}
