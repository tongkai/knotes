package com.tk.kainotes.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.EditText;

import com.tk.kainotes.R;
import com.tk.kainotes.utils.ToolbarUtil;

public class WritingActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    private EditText edtNotes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_writing);

        mToolbar = (Toolbar) findViewById(R.id.id_toolbar);
        ToolbarUtil.initToolbar(this, mToolbar, R.string.str_write);

        edtNotes = (EditText) findViewById(R.id.notes_edt);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            //实现点击toolbar返回事件
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
