package com.tk.kainotes.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tk.kainotes.R;
import com.tk.kainotes.adapter.NotesAdapter;
import com.tk.kainotes.model.SynopsisNotes;
import com.tk.kainotes.ui.WritingActivity;
import com.tk.kainotes.ui.decorations.LinearDecoration;

import java.util.ArrayList;
import java.util.List;


public class NotesFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private RecyclerView mRecyclerView;
    private NotesAdapter mAdapter;
    private FloatingActionButton mFloatingActionButton;

    private List<SynopsisNotes> list;



    public NotesFragment() {
        Log.d("TAG",this.toString());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notes, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.notes_recyclerview);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(new NotesAdapter(getActivity(),list));
        mRecyclerView.addItemDecoration(new LinearDecoration(getActivity(),LinearDecoration.VERTICAL_LIST));

        mFloatingActionButton = (FloatingActionButton) view.findViewById(R.id.id_fab);
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startWriteNote();
            }
        });
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }



    private void initData(){
        list = new ArrayList<SynopsisNotes>();
        SynopsisNotes synNotes = null;
        for (int i = 0;i < 10;i++){
            synNotes = new SynopsisNotes("TAG"+i,"2015-09-"+(i+10));
            list.add(synNotes);
        }
    }

    private void startWriteNote(){
        startActivity(new Intent(this.getActivity(), WritingActivity.class));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("TAG",this.toString()+"---destroy");
    }
}
