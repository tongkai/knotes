package com.tk.kainotes.utils;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.tk.kainotes.R;

/**
 * Created by GEC-IOS100 on 15/11/7.
 */
public class ToolbarUtil {
    private static final String TAG = ToolbarUtil.class.getSimpleName();
    //内存泄露风险
    public static void initToolbar(AppCompatActivity activity,Toolbar toolbar){

        initToolbar(activity,toolbar,R.string.app_name);

    }

    public static void initToolbar(AppCompatActivity activity,Toolbar toolbar,int res){
        if (activity == null || toolbar == null){
            Log.e(TAG,activity.getClass().getSimpleName()+"OR toolbar maybe NULL!");
            return;
        }

        activity.setSupportActionBar(toolbar);
        toolbar.setTitle(res);

        if (activity.getSupportActionBar() != null){
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);//给home icon的左边加上一个返回的图标
            activity.getSupportActionBar().setHomeButtonEnabled(true);//需要api level 14  使用home-icon 可点击
        }

    }
}
