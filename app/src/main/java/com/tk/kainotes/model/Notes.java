package com.tk.kainotes.model;

import java.util.Date;

/**
 * Created by GEC-IOS100 on 15/10/31.
 */
public class Notes {
    String content;
    Date time;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
