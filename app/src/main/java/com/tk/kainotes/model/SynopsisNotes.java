package com.tk.kainotes.model;

/**
 * Created by GEC-IOS100 on 15/11/4.
 * 简略内容
 */
public class SynopsisNotes {
    String synopsis;
    String date;

    public SynopsisNotes(){}
    public SynopsisNotes(String synopsis,String date){
        this.synopsis = synopsis;
        this.date = date;
    }
    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
